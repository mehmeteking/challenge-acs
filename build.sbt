name := "acs"

version := "1.0"

scalaVersion := "2.11.5"

libraryDependencies ++= Seq(
  "org.specs2"        %% "specs2-core" % "2.4.16" % Test,
  "org.specs2"        %% "specs2-mock" % "2.4.16" % Test
)
