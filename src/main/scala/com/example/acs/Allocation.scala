package com.example.acs

case class Allocation(id: String, employee: String, booking: String, status: Allocation.Status)
object Allocation {

  sealed trait Status
  object Status {

    case object Allocated extends Status
    case object InProgress extends Status
    case object Finished extends Status

  }

}
