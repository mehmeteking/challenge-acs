package com.example.acs

import java.util.Date

case class Job(id: String, name: String, customer: String, start: Option[Date], end: Option[Date], status: Job.Status)
object Job {
  import Allocation.{ Status => AStatus }

  type JobAlloc = (Job, Seq[AStatus])

  sealed trait Status
  object Status {

    case object Queued extends Status
    case object PendingAllocation extends Status
    case object Ready extends Status
    case object InProgress extends Status
    case object Finished extends Status

  }

  def calculateJobStatus(job: Job, allocations: Seq[Allocation]) = calculateOnPattern(job, allocations.map(_.status))

  implicit class JobAllocWrapper(val jobAlloc: JobAlloc) extends AnyVal {
    def job = jobAlloc._1
    def allocations = jobAlloc._2
  }

  private def calculateOnPattern(jobAlloc: JobAlloc) = jobAlloc match {
    case AlreadyDone() => Status.Finished
    case NoTime() => Status.Queued
    case NotAllocated() => Status.PendingAllocation
    case NotStarted() => Status.Ready
    case Completed() => Status.Finished
    case _ => Status.InProgress
  }

  object NoTime {
    def unapply(jobAlloc: JobAlloc) = !jobAlloc.job.start.isDefined
  }

  object NotAllocated {
    def unapply(jobAlloc: JobAlloc) = jobAlloc.allocations.isEmpty
  }

  object NotStarted {
    def unapply(jobAlloc: JobAlloc) = jobAlloc.allocations.forall(_ == AStatus.Allocated)
  }

  object AlreadyDone {
    def unapply(jobAlloc: JobAlloc) = jobAlloc.job.status == Status.Finished
  }

  object Completed {
    val allDone: Seq[AStatus] => Boolean = _.forall(_ == AStatus.Finished)
    def unapply(jobAlloc: JobAlloc) = jobAlloc.allocations.forall(_ == AStatus.Finished)
  }

}
