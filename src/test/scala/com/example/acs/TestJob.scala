package com.example.acs

class TestJob extends org.specs2.mutable.Specification {
  import TestUtil._
  import Job.{ Status => Stt }
  import Allocation.{ Status => AStt }

  "calculateJobStatus" should {

    def verify(job: Job, allocs: Seq[Allocation], expected: Stt) = {
      Job.calculateJobStatus(job, allocs) must beEqualTo(expected)
    }

    "return Finished when" in {

      "job is already completed" in {
        verify(mockJob(status = Stt.Finished), Seq.empty, Stt.Finished)
      }

      "all allocations are done" in {
        verify(mockJob(status = Stt.InProgress, start = Some(now)), mockAlloc(status = AStt.Finished) :: Nil, Stt.Finished)
      }
    }

    "return Queued when" in {

      "time is not set" in {
        verify(mockJob(status = Stt.Ready), Seq.empty, Stt.Queued)
      }

    }

    "return Ready when" in {

      "allocations have not started" in {
        verify(mockJob(status = Stt.Queued, start = Some(now)), mockAlloc(status = AStt.Allocated) :: Nil, Stt.Ready)
      }

    }

    "return InProgress when" in {

      "there is allocation in progress" in {
        verify(mockJob(status = Stt.Queued, start = Some(now)), mockAlloc(status = AStt.InProgress) :: Nil, Stt.InProgress)
      }

    }

    "return PendingAllocation when" in {

      "allocations need to be made" in {
        verify(mockJob(status = Stt.Queued, start = Some(now)), Seq.empty, Stt.PendingAllocation)
      }

    }
  }
}
