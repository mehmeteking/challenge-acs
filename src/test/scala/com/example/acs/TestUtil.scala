package com.example.acs

import java.util.Date

object TestUtil extends org.specs2.mock.Mockito {

  def now = new Date()

  def mockJob(id: String = "", name: String = "", customer: String = "",
              start: Option[Date] = None, end: Option[Date] = None, status: Job.Status) = {
    val job = mock[Job]
    job.id returns id
    job.name returns name
    job.start returns start
    job.end returns end
    job.customer returns customer
    job.status returns status
    job
  }

  def mockAlloc(id: String = "", employee: String = "", booking: String = "", status: Allocation.Status) = {
    val alc = mock[Allocation]
    alc.id returns id
    alc.employee returns employee
    alc.booking returns booking
    alc.status returns status
    alc
  }

}
